import javax.lang.model.util.ElementScanner6;

public class NetCalc{
    enum State { DIGIT, DOTORSLASH, PREFIX}
    public static void main(String [] Args){
        System.out.println("IPV4 Calculator!!!");
        switch(Args[0]){
            case "prefix":
                int pref=Integer.parseInt(Args[1]);
                System.out.println("Prefix /" + pref);
                for(int i=0; i<4; i++){

//                    int x=255<<((pref>7)?0:8-pref);

                    if(pref>=8){
                        System.out.print("255.");
                    }else{
                        int x=(0b11111111 << (8 - pref ))&0b11111111;
                        //int x=255 - (0b11111111 >> pref );
                       // int x=0b1111111100000000 >> pref ;
                        System.out.print(x + ".");
                    }
                    pref=pref-8;
                    if(pref<0) pref=0;
                }
                break;
            case "network": //   192.168.3.56/24
                String in_net=Args[1];
                State state=State.DIGIT;
                int [] ipa = {0,0,0,0}; //new int[4];
                int pos = 0;
                int prefix=0;

                for(int i=0;i<in_net.length();i++){     //   str[34]
                    char simbol=("0123456789".indexOf(in_net.charAt(i)) !=-1 )? '0' :   in_net.charAt(i);
                    System.out.println("d" + simbol + ' ' + state);
                    switch(state){
                        case DOTORSLASH:
                            if(pos<3){
                                if(simbol == '.' ){
                                    pos++;
                                    state=State.DIGIT;
                                    break;
                                }
                            }else
                                if( simbol == '/') {
                                    state=State.PREFIX;
                                    break;
                                }
                        case DIGIT:
                            if(simbol == '0'){
                                ipa[pos]*=10;
                                ipa[pos]+=in_net.charAt(i) - '0';
                            }else 
                                System.exit(1);
                            state=State.DOTORSLASH;
                            break;
                        case PREFIX:
                            if(simbol == '0'){
                                prefix*=10;
                                prefix+=in_net.charAt(i) - '0';
                            }else
                                System.exit(2);
                        break;
                        default: 
                            break;
                    }

                }
                for(int oct:ipa)
                    System.out.println(oct);
                System.out.println(prefix);
            default:
                System.out.println("please use NetCalc prefix number(0-32)");
            break;
        }
    }
}

